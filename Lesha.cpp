// Lesha.cpp : Defines the entry point for the application.
//

#include "pch.h"
#include "framework.h"
#include "Lesha.h"
#include <cmath>
#include <string>

#include <objidl.h>
#include <gdiplus.h>
#include "polynom.hpp"

using namespace Gdiplus;
using namespace std;

#pragma comment (lib,"Gdiplus.lib")

#define MAX_LOADSTRING 100

struct POINT_INPUT
{
	HWND label;
	HWND x;
	HWND y;
};

#define POINT_INPUT_LABEL	0x0000
#define POINT_INPUT_X		0x0001
#define POINT_INPUT_Y		0x0002

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

const long pointsCount = 10;
const long rows = 20;
const long columns = pointsCount / rows + ((pointsCount % rows > 0) ? 1 : 0);

const long rowHeight = 20;
const long columnSpaceSize = 20;
const SIZE labelSize{ 30, rowHeight };
const SIZE ySize    { 35, rowHeight };
const SIZE RowSize  { labelSize.cx + 10 + ySize.cx, rowHeight };

const SIZE inputRegionSize{ CALCULATE_TOTAL_SIZE(RowSize.cx, columnSpaceSize, columns), CALCULATE_TOTAL_SIZE(RowSize.cy, 10, rows) };
const SIZE graphRegionSize{ inputRegionSize.cy * 2, inputRegionSize.cy };
const SIZE contentSize{ inputRegionSize.cx + columnSpaceSize + graphRegionSize.cx, inputRegionSize.cy };

POINT points[pointsCount];
POINT_INPUT pointsInputs[pointsCount];

const long pointRadius = 5;

HFONT hFont;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;

	hFont = CreateFont(20, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Tahoma");

	for (UINT i = 0; i < pointsCount; i++)
	{
		points[i].x = graphRegionSize.cx / (pointsCount - 1) * i;
	}
	
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, 0);
	
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LESHA, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }
	
    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LESHA));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	GdiplusShutdown(gdiplusToken);
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LESHA));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_LESHA);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable
	
	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, 8 + 10 + contentSize.cx + 10 + 8, 51 + 10 + contentSize.cy + 10 + 8, nullptr, nullptr, hInstance, nullptr);
	
	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

void DrawGraph(HDC hdc)
{
	const Rect rect = Rect(10 + inputRegionSize.cx + columnSpaceSize, 10, graphRegionSize.cx, graphRegionSize.cy);

	Graphics graphics(hdc);
	
	SolidBrush pointBrush(Color(255, 255, 0, 0));
	Pen PointBorderPen(Color(255, 200, 0, 0), 2);
	
	Pen linePen(Color(255, 0, 0, 255), 2);
	
	Pen borderPen(Color(255, 0, 0, 0), 1);
	
	Pen Grid0Pen(Color(255, 50, 50, 50), 2);
	Pen Grid1Pen(Color(255, 50, 50, 50), 1);
	Pen Grid2Pen(Color(255, 185, 185, 185), 1);

	long centerX = rect.Width / 2;
	long centerY = rect.Height / 2;

	vector<POINT> curveIn = vector<POINT>(begin(points), end(points));
	vector<Point> curveOut;
	polynom(curveIn, curveOut, 0, graphRegionSize.cx, 1);
	for (auto& point : curveOut)
	{
		point.Y *= -1;
		
		point.X += rect.X;
		point.Y += rect.Y + centerY;
	}

	graphics.SetClip(Rect(rect.X, rect.Y, rect.Width + 1, rect.Height + 1), CombineMode::CombineModeIntersect);

	for (int i = 10; i < rect.Width; i += 10) // vertical 10 lines
	{
		graphics.DrawLine(&Grid2Pen, rect.X + i, rect.Y, rect.X + i, rect.Y + rect.Height);
	}

	for (int i = 10; i < rect.Height / 2; i += 10) // horizontal 10 lines
	{
		graphics.DrawLine(&Grid2Pen, rect.X, rect.Y + centerY + i, rect.X + rect.Width, rect.Y + centerY + i);
		graphics.DrawLine(&Grid2Pen, rect.X, rect.Y + centerY - i, rect.X + rect.Width, rect.Y + centerY - i);
	}

	for (int i = 100; i < rect.Width; i += 100) // vertical 100 lines
	{
		graphics.DrawLine(&Grid1Pen, rect.X + i, rect.Y, rect.X + i, rect.Y + rect.Height);
	}

	for (int i = 100; i < rect.Height / 2; i += 100) // horizontal 100 lines
	{
		graphics.DrawLine(&Grid1Pen, rect.X, rect.Y + centerY + i, rect.X + rect.Width, rect.Y + centerY + i);
		graphics.DrawLine(&Grid1Pen, rect.X, rect.Y + centerY - i, rect.X + rect.Width, rect.Y + centerY - i);
	}

	graphics.DrawLine(&Grid0Pen, rect.X, rect.Y + centerY, rect.X + rect.Width, rect.Y + centerY);

	graphics.DrawLines(&linePen, &*curveOut.begin(), curveOut.size());
	
	POINT npoints[pointsCount];
	for (UINT i = 0; i < pointsCount; i++)
	{
		POINT np = POINT{ rect.X + points[i].x, rect.Y + centerY - points[i].y };
		graphics.FillEllipse(&pointBrush, np.x - pointRadius, np.y - pointRadius, pointRadius * 2, pointRadius * 2);
		graphics.DrawEllipse(&PointBorderPen, np.x - pointRadius, np.y - pointRadius, pointRadius * 2, pointRadius * 2);
	}

	graphics.DrawRectangle(&borderPen, rect);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int t;
    switch (message)
    {
	case WM_CREATE:
		for (UINT i = 0; i < pointsCount; i++)
		{
			long x = i / rows;
			long y = i % rows;
			
			HWND p_label = CreateWindowA("STATIC", 0, WS_CHILD | WS_VISIBLE | SS_CENTER, 10 + (columnSpaceSize + RowSize.cx) * x, 10 + (10 + RowSize.cy) * y, labelSize.cx, labelSize.cy, hWnd, HMENU(i * 3 + 0), hInst, 0);
			pointsInputs[i].label = p_label;
			SetWindowTextA(p_label, to_string(i + 1).c_str());
			SendMessage(p_label, WM_SETFONT, (WPARAM)hFont, 0);

			HWND p_y = CreateWindowA("EDIT", 0, WS_BORDER | WS_CHILD | WS_VISIBLE, 10 + labelSize.cx + 10 + (columnSpaceSize + RowSize.cx) * x, 10 + (10 + RowSize.cy) * y, ySize.cx, ySize.cy, hWnd, HMENU(i * 3 + 2), hInst, 0);
			pointsInputs[i].y = p_y;
			SetWindowTextA(p_y, to_string(points[i].y).c_str());
		}
		break;
    case WM_COMMAND:
        {
			auto hi = HIWORD(wParam);
			auto lo = LOWORD(wParam);

			switch (hi)
			{
			case EN_UPDATE:
				if (lo < pointsCount * 3)
				{
					auto pointId = lo / 3;
					auto propId = lo % 3;

					char str[5];
					int len;

					switch (propId)
					{
					case POINT_INPUT_X:
						len = GetWindowTextLengthA(pointsInputs[pointId].x);
						if (len > 0)
						{
							GetWindowTextA(pointsInputs[pointId].x, str, 5);
							if (isNumber(str))
							{
								points[pointId].x = stoi(str);
							}
							else
							{
								string newStr;
								FetchNumber(str, newStr);
								SetWindowTextA(pointsInputs[pointId].x, newStr.c_str());
							}
						}
						else
						{
							SetWindowTextA(pointsInputs[pointId].x, "0");
						}
						break;
					case POINT_INPUT_Y:
						len = GetWindowTextLengthA(pointsInputs[pointId].y);
						if (len > 0)
						{
							GetWindowTextA(pointsInputs[pointId].y, str, 5);
							if (isNumber(str))
							{
								points[pointId].y = stoi(str);
							}
							else
							{
								string newStr;
								FetchNumber(str, newStr);
								SetWindowTextA(pointsInputs[pointId].y, newStr.c_str());
							}
						}
						else
						{
							SetWindowTextA(pointsInputs[pointId].y, "0");
						}
						break;
					}
				}

				InvalidateRect(hWnd, new RECT{ 10 + inputRegionSize.cx + columnSpaceSize, 10, 10 + inputRegionSize.cx + columnSpaceSize + graphRegionSize.cx, 10 + graphRegionSize.cy }, true);
				break;
			default:
				switch (lo)
				{
				case IDM_ABOUT:
					DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
					break;
				case IDM_EXIT:
					DestroyWindow(hWnd);
					break;
				default:
					return DefWindowProc(hWnd, message, wParam, lParam);
				}
			}
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			DrawGraph(hdc);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
	
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
