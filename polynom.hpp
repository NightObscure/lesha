﻿#pragma once

#include "pch.h"

using namespace std;

double u_cal(double u, int n)
{
	double temp = u;
	for (int i = 1; i < n; i++) temp *= (u + i);
	return temp;
}

double fact(int n)
{
	double f = 1.;
	for (int i = 2; i <= n; i++) f *= i;
	return f;
}

void polynom(vector<POINT> in, vector<Gdiplus::Point>& out, int from, int to, int step)
{
	out.clear();

	const int n = in.size();
	
	double** y = new double*[n];
	for (int i = 0; i < n; i++)
	{
		y[i] = new double[n];
	}
	
	for (int i = 0; i < n; i++) y[i][0] = in[i].y;
	for (int i = 1; i < n; i++)
	{
		for (int j = n - 1; j >= i; j--) y[j][i] = y[j][i - 1] - y[j - 1][i - 1];
	}

	int i = 0;
	double x1 = from;
	double x2 = to;
	double dx = step;
	for (double x = x1; x <= x2; x += dx)
	{
		double sum = y[n - 1][0];
		double u = (x - double(in[n - 1].x)) / double(in[1].x - in[0].x);
		for (int i = 1; i < n; i++)
		{
			sum += (u_cal(u, i) * y[n - 1][i]) / fact(i);
		}

		out.push_back(Gdiplus::Point{(INT)x, (INT)sum});
	}
}