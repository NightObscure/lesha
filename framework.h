#pragma once

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <string>
#include <vector>

using namespace std;

#define CALCULATE_TOTAL_SIZE(itemSize, spaceSize, count)	((itemSize) * (count) + (spaceSize) * ((count) - 1))

inline void FetchNumber(string inStr, string& outStr)
{
	outStr = "";

	for (int i = 0; i < inStr.length(); i++)
	{
		if ((i == 0 && (inStr[0] == '-' || inStr[0] == '+') || isdigit(inStr[i]))) outStr += inStr[i];
	}
}

inline bool isNumber(string str)
{
	for (int i = 0; i < str.length(); i++)
	{
		if (!(i == 0 && (str[0] == '-' || str[0] == '+') || isdigit(str[i]))) return false;
	}
	return true;
}
